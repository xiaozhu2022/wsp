//部署测试
let arg2 = process.argv[2] || "";
if (arg2 == "--test") {
  console.log('测试通过');
  process.exit(0); //退出，不然会让面板继续运行
}
if (arg2 == "--help") {
  console.log('WSP的帮助信息');
  console.log('用法：');
  console.log('[node] app.js [--help] [--test]');
  console.log('选项：');
  console.log('help  显示帮助信息');
  console.log('test  进行全自动部署测试，不会保留现有的服务');
  process.exit(0); //退出，不然会让面板继续运行
}