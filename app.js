/* eslint-disable no-unused-vars */
//运行时环境检测
try {
  let versionNum = parseInt(process.version.replace(/v/gim, "").split(".")[0]);
  //我们建议最低版本为 v10 版本
  if (versionNum < 10) {
    console.log("[Node Test] 您的 Node 运行环境版本似乎低于我们要求的版本.");
    console.log("[Node Test] 可能会出现未知情况,建议您更新 Node 版本 (>=10.0.0)");
  }
} catch (err) {
  //忽略任何版本检测导致的错误
}
//监听的端口
var PORT = 8888;
//监听的IP
var IP = '127.0.0.1';


const httpd = require("http");
const fs = require("fs");
var WSP_ver = '1.0.0'
require("./config/help");

//-----------LOGO/标题 输出-----------
const LOGO_FILE_PATH = "./config/logo.txt";
let data = fs.readFileSync(LOGO_FILE_PATH, "utf-8");
console.log(data);
process.title = 'Web Server Panel 版本' + WSP_ver;
//----------- WSP信息 ------------
console.log('[WSP]当前WSP版本: ' + WSP_ver);
console.log('[WSP]Web 目录: ' + process.cwd() + '/www');
//-----------服务器信息-----------
// 输出当前目录
console.log('[Node Test]当前运行目录: ' + process.cwd());
// 输出当前node版本
console.log('[Node Test]当前Node.js版本: ' + process.version);
// 获取执行路径
console.log('[Node Test]当前Node.js执行文件路径 : ' + process.execPath);
// 平台信息
console.log('[Node Test]当前Node.js运行平台:' + process.platform);


//-----------初始化-----------
console.log('[WSP]正在初始化…');
console.log('[WSP]初始化完成！即将创建服务');

//-----------创建服务-----------
httpd.createServer((req,res)=>{
    //读取www文件夹下的路径
    fs.readFile(`www${req.url}`,(err,data)=>{
        //失败返回404
        if(err){
　　　　　　　　 res.writeHeader(404);
            res.write('<script>window.location.replace("./error/404.html")</script>');
        }else{
            //成功返回页面
            res.write(data);
        }
        res.end();
    })
}).listen(PORT);
console.log("[HTTP] HTTP 模块正在监听 http://" + IP + ":" + PORT + "/");
console.log("[INFO] Web托管面板已经启动");
console.log("[HTTP]访问你的主页 http://" + IP + ":" + PORT + "/index.html");

//-----------退出相关事件-----------
let _endFlag = false;
process.on("SIGINT", function () {
  if (_endFlag) return;
  _endFlag = true;
  console.log("[EXIT]面板正在结束与回收资源");

  // 异步等待1秒，面板自动结束
  setTimeout(() => {
    console.log("[EXIT]正在退出...");
    process.exit(0);
  }, 1000);
});
/**
 *----------------------
 * 开源许可证 : WTFPL
 * 利用WSP做的事情将与原作者和贡献者们无关
 *----------------------
 */